<?php

namespace Drupal\betasite\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a 'Hostname' condition.
 *
 * @Condition(
 *   id = "condition_plugins_hostname",
 *   label = @Translation("Hostname"),
 * )
 */
class BetaSiteHostname extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs a BetaSiteHostname plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'hostnames' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['hostnames'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Hostname'),
      '#default_value' => $this->configuration['hostnames'],
      '#description' => $this->t("Specify hostnames. Enter one hostname per line."),
    ];

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['hostnames'] = $form_state->getValue('hostnames');

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    $hostnames = $this->getNeededHostnames();
    $hostnames = implode(', ', $hostnames);
    if (!empty($this->configuration['negate'])) {
      return $this->t('Do not return true if the current request does not have any of the: @hostnames.', [
        '@hostnames' => $hostnames,
      ]);
    }
    return $this->t('Return true if the current request has any of the hostnames: @hostnames.', [
      '@hostnames' => $hostnames,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $needed_hostnames = $this->getNeededHostnames();
    if (!$needed_hostnames) {
      return TRUE;
    }

    $current_hostname = $this->requestStack->getCurrentRequest()->getHost();

    return (in_array($current_hostname, $needed_hostnames)) ? TRUE : FALSE;

  }

  /**
   * Returns the needed parameters from this plugin configuration.
   *
   * @return array
   *   The needed parameters array.
   */
  private function getNeededHostnames() {
    return array_map('trim', explode(PHP_EOL, mb_strtolower($this->configuration['hostnames'])));
  }

}
