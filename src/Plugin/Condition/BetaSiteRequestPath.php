<?php

namespace Drupal\betasite\Plugin\Condition;

use Drupal\system\Plugin\Condition\RequestPath;

/**
 * Override the request path plugin.
 *
 * Use the original path alias when checking against the aliases in the
 * Block visibility settings.
 */
class BetaSiteRequestPath extends RequestPath {

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    // Convert path to lowercase. This allows comparison of the same path
    // with different case. Ex: /Page, /page, /PAGE.
    $pages = mb_strtolower($this->configuration['pages']);
    if (!$pages) {
      return TRUE;
    }

    $request = $this->requestStack->getCurrentRequest();
    // Compare the lowercase path alias (if any) and internal path.
    $path = $this->currentPath->getPath($request);
    // Do not trim a trailing slash if that is the complete path.
    $path = $path === '/' ? $path : rtrim($path, '/');

    $path_alias = mb_strtolower($this->aliasManager->getAliasByPath($path));

    // If we're on the beta site, compare using the original alias.
    // Otherwise, it will use the beta alias, which may not match blocks.
    $betaSiteManager = \Drupal::service('betasite.manager');
    if ($betaSiteManager->isBetaSite()) {
      $query = \Drupal::database()->select('path_alias', 'pa');
      $query->condition('pa.beta_alias', $path_alias);
      $query->addField('pa', 'path');
      $query->range(0, 1);
      $original_alias = $query->execute()->fetchField();

      if ($original_alias !== FALSE) {
        $path_alias = $original_alias;
      }
    }

    return $this->pathMatcher->matchPath($path_alias, $pages) || (($path != $path_alias) && $this->pathMatcher->matchPath($path, $pages));
  }

}
