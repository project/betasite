<?php

namespace Drupal\betasite\PathProcessor;

use Drupal\betasite\BetaAliasStorage;
use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Processes the outbound paths.
 */
class BetaSitePathProcessor implements OutboundPathProcessorInterface {

  /**
   * A request stack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Entity type manager.
   *
   * @var \Drupal\betasite\BetaAliasStorage
   */
  protected $betaAliasStorage;

  /**
   * Constructs a CmsBetaLinksPathProcessor object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   A request stack object.
   * @param \Drupal\betasite\BetaAliasStorage $beta_alias_storage
   *   Beta alias storage.
   */
  public function __construct(RequestStack $request_stack, BetaAliasStorage $beta_alias_storage) {
    $this->requestStack = $request_stack;
    $this->betaAliasStorage = $beta_alias_storage;
  }

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    $baseUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    if ((strpos($baseUrl, 'https://beta.') === 0 || strpos($baseUrl, 'http://beta.') === 0) && $betaAlias = $this->betaAliasStorage->getAlias($path)) {
      $path = $betaAlias;
    }
    return $path;
  }

}
