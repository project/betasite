<?php

namespace Drupal\betasite\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\mysql\Driver\Database\mysql\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Listens to the dynamic route events.
 */
class BetaSiteRouteSubscriber implements ContainerInjectionInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the BetaSiteRouteSubscriber class.
   *
   * @param \Drupal\mysql\Driver\Database\mysql\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    // $route_collection = new RouteCollection();
    $aliases = $this->database->query("SELECT * FROM path_alias WHERE beta_alias IS NOT NULL AND beta_alias != ''")->fetchAll();
    $routes = [];
    foreach ($aliases as $alias) {
      if (!empty($alias->path) && $nid = explode('/', $alias->path)) {
        if (!empty($nid[2])) {
          // Load the node as a way to validate the NID.
          $node = $this->entityTypeManager->getStorage('node')->load($nid[2]);
          if ($node) {
            $routes[] = new Route(
              $alias->beta_alias, /* @phpstan-ignore-line */
              [
                '_controller' => '\Drupal\node\Controller\NodeViewController::view',
                '_title_callback' => '\Drupal\node\Controller\NodeViewController::title',
                'node' => $node->id(),
              ],
              [
                '_permission'  => 'access content',
              ]
            );
          }
        }
      }
    }
    return $routes;
  }

}
