<?php

namespace Drupal\betasite\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Gets Beta Site Manager.
 */
class BetaSiteManager implements BetaSiteManagerInterface {

  use StringTranslationTrait;

  /**
   * Database Service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requeststack;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $betaconfig;

  /**
   * Object constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requeststack
   *   Request Stack Service.
   */
  public function __construct(
    RequestStack $requeststack
  ) {
    $this->requeststack = $requeststack;
  }

  /**
   * {@inheritdoc}
   */
  public function isBetaSite() {
    $host = $this->requeststack->getCurrentRequest()->getHost();
    $parts = explode('.', $host);
    if ($parts[0] === 'beta') {
      return TRUE;
    }
    return FALSE;
  }

}
