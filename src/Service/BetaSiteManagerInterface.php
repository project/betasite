<?php

namespace Drupal\betasite\Service;

/**
 * Defines Beta Site manager service.
 */
interface BetaSiteManagerInterface {

  /**
   * Check if user is on beta site.
   *
   * @return bool
   *   True if user is going to the beta domain.
   */
  public function isBetaSite();

}
