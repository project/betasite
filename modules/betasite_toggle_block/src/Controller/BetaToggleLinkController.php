<?php

namespace Drupal\betasite_toggle_block\Controller;

use Drupal\betasite\BetaAliasStorage;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Laminas\Diactoros\Response\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller for AJAX call to retrieve beta or standard url.
 */
class BetaToggleLinkController extends ControllerBase {

  /**
   * The injected current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private Request $request;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $db;

  /**
   * The injected BetaAliasStorage service.
   *
   * @var \Drupal\betasite\BetaAliasStorage
   */
  private BetaAliasStorage $betaAliasStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, Connection $db, BetaAliasStorage $beta_alias_storage,) {
    $this->request = $request_stack->getCurrentRequest();
    $this->db = $db;
    $this->betaAliasStorage = $beta_alias_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('database'),
      $container->get('betasite.beta_alias_storage'),
    );
  }

  /**
   * Retrieves the beta or standard alternative url to the given url.
   */
  public function getToggleLink() {
    $target_domain = $this->request->get('domain');
    $old_alias = $this->request->get('path');

    if ($target_domain === 'beta') {
      $new_alias = $this->getBetaAlias($old_alias);
    }
    else {
      $new_alias = $this->getStandardAlias($old_alias);
    }

    return new JsonResponse([
      'path' => $new_alias,
      'status' => 200,
    ]);
  }

  /**
   * Translates a standard alias to a beta alias.
   *
   * @param string $standard_alias
   *   The standard alias.
   *
   * @return string
   *   The beta alias.
   */
  private function getBetaAlias(string $standard_alias) {
    $storage_result = $this->betaAliasStorage->getAlias($standard_alias);
    // If no beta alias is stored, default to standard.
    return empty($storage_result) ? $standard_alias : $storage_result;
  }

  /**
   * Translates a beta alias to a standard alias.
   *
   * @param string $beta_alias
   *   The beta alias.
   *
   * @return string
   *   The standard alias.
   */
  private function getStandardAlias(string $beta_alias) {
    $result = $this->db->query(
      'SELECT alias
         FROM {path_alias}
         WHERE beta_alias = :alias
         AND langcode = :lang',
      [':alias' => $beta_alias, ':lang' => 'en']);

    $alias = $result->fetchField(0);
    // If no beta alias is stored, default to standard.
    return empty($alias) ? $beta_alias : $alias;
  }

}
