<?php

namespace Drupal\betasite_toggle_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a block with a link to the beta site.
 *
 * @Block(
 *   id = "betasite_toggle_block",
 *   admin_label = @Translation("Beta toggle"),
 *   category = @Translation("Beta")
 * )
 */
class ToggleBetaDomainBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'beta_toggle',
      '#attached' => [
        'library' => [
          'betasite_toggle_block/toggle_beta_link',
        ],
      ],
    ];
  }

}
