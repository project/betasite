(function (Drupal, window) {
  'use strict';

  /**
   * Contains the logic to fill the information into the toggle block.f
   *
   * Queries BetaToggleLinkController class on server.
   *
   * @returns object
   */
  const betaLinkBuilder = function() {
    let this_site, that_site, target_host,
    this_site_element, that_site_element,
    ajaxRequest = new XMLHttpRequest(),
    ready_to_go = false;

    /**
     * Locates the tags to fill information into and determines target host.
     *
     * @returns {boolean}
     *   If code was able to find the appropriate elements or not.
     */
    const init = function() {
      this_site_element = document.getElementById('this-site');
      that_site_element = document.getElementById('that-site');
      if (this_site_element === null || that_site_element === null) {
        return false;
      }
      determineCurrentHost();
      return true;
    }

    /**
     * Sets this_site and that_site to current domain and target domain.
     * Saves target_host for use later.
     */
    const determineCurrentHost = function() {
      let host_parts = window.location.hostname.split('.');
      if (host_parts[0] === 'beta') {
        this_site = 'beta';
        host_parts.shift();
        if (host_parts[0] === 'cms') {
          host_parts.unshift('www');
        }
      }
      else {
        this_site = 'standard';
        if (host_parts[0] === 'www') {
          host_parts.shift();
        }
        host_parts.unshift('beta');
      }

      that_site = this_site === 'standard' ? 'beta' : 'standard';
      target_host = host_parts.join('.');
    }

    /**
     * Actually sets the text in the block.
     */
    const setToggleText = function() {
      if (!ready_to_go) {
        return;
      }
      if (this_site === 'standard') {
        this_site_element.textContent = "The week of September 5, we'll launch an updated homepage, streamlined navigation, and improved search features on CMS.gov.";
        that_site_element.textContent = "Visit beta.cms.gov to preview the improvements and share your feedback.";
      }
      else {
        this_site_element.textContent = "You're currently previewing the updated CMS.gov that will officially launch the week of September 5. Take some time to explore our updated homepage, streamlined navigation, and improved search. Share your feedback using the blue tab on the right side of the screen.";
        that_site_element.textContent = "Go back to CMS.gov.";
      }
      if (!shortcutHomepage()) {
        getUrlFromServer();
      }
    }

    /**
     * If on homepage, no need to do path lookup.
     *
     * @returns {boolean}
     *   Whether current page is homepage or not.
     */
    const shortcutHomepage = function() {
      if (window.location.pathname === '/' && target_host) {
        setToggleUrl('https://' + target_host);
        return true;
      }
      return false;
    }

    /**
     * Sends request to get translated path from server.
     */
    const getUrlFromServer = function() {
      ajaxRequest.onreadystatechange = handleResponse;
      ajaxRequest.open('POST', '/beta-link', true);
      ajaxRequest.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      ajaxRequest.send('domain=' + that_site + '&path=' + window.location.pathname);
    }

    /**
     * Sets url in link based on response from BetaToggleLinkController.
     */
    const handleResponse = function() {
      if (ajaxRequest.readyState === XMLHttpRequest.DONE) {
        if (ajaxRequest.status === 200) {
          setToggleUrl('https://' + target_host);
        }
        else {
          window.console.error("Couldn't determine beta toggle link in toggle block.");
        }
      }
    }

    /**
     * Helper function to set the url in link.
     *
     * @param url
     *   The url to set as href.
     */
    const setToggleUrl = function(url) {
      that_site_element.setAttribute('href', url);
    }

    // Initialize variables and pass public method out.
    ready_to_go = init();
    return { 'setToggleText': setToggleText}
  }

  document.addEventListener(
    'DOMContentLoaded',
    () => { betaLinkBuilder().setToggleText() }
  );
})(Drupal, window);
