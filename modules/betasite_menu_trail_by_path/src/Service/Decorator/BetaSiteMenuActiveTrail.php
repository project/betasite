<?php

namespace Drupal\betasite_menu_trail_by_path\Service\Decorator;

use Drupal\menu_trail_by_path\MenuTrailByPathActiveTrail;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\betasite\Service\BetaSiteManagerInterface;

/**
 * A Decorator for MenuTrailByPathActiveTrail.
 */
class BetaSiteMenuActiveTrail extends MenuTrailByPathActiveTrail {

  /**
   * Path Alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $pathAliasManager;

  /**
   * Current Path Stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  protected $currentPathStack;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Beta Site Manager.
   *
   * @var \Drupal\betasite\Service\BetaSiteManagerInterface
   */
  protected $betaSiteManager;

  /**
   * MenuTrailByPathActiveTrail constructor.
   *
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   Menu Link Manager Service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route Match Service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Backend Cache Service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   Backend Lock Service.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   Path Validation service.
   * @param \Drupal\Core\Routing\RequestContext $context
   *   Request Context Service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager Service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory Service.
   * @param \Drupal\path_alias\AliasManagerInterface $pathAliasManager
   *   Path Alias Manager Service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   Current Path Stack.
   * @param \Drupal\Core\Database\Connection $database
   *   Database Connection Service.
   * @param \Drupal\betasite\Service\BetaSiteManagerInterface $betaSiteManager
   *   Beta Site Manager Service.
   */
  public function __construct(MenuLinkManagerInterface $menu_link_manager, RouteMatchInterface $route_match, CacheBackendInterface $cache, LockBackendInterface $lock, PathValidatorInterface $path_validator, RequestContext $context, LanguageManagerInterface $languageManager, ConfigFactoryInterface $config_factory, AliasManagerInterface $pathAliasManager, CurrentPathStack $currentPathStack, Connection $database, BetaSiteManagerInterface $betaSiteManager) {
    parent::__construct($menu_link_manager, $route_match, $cache, $lock, $path_validator, $context, $languageManager, $config_factory);
    $this->database = $database;
    $this->pathAliasManager = $pathAliasManager;
    $this->currentPathStack = $currentPathStack;
    $this->betaSiteManager = $betaSiteManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function doGetActiveTrailIds($menu_name) {
    // Parent ids; used both as key and value to ensure uniqueness.
    // We always want all the top-level links with parent == ''.
    $active_trail = parent::doGetActiveTrailIds($menu_name);

    // If trail has valid element or non-beta, then just return the trail.
    if ((count($active_trail) && !empty(array_key_first($active_trail))) ||
        (!$this->betaSiteManager->isBetaSite())) {
      return $active_trail;
    };

    // Check for the current path and see if it is an alias.
    $current_path = $this->currentPathStack->getPath();
    $internalpath = $this->pathAliasManager->getPathByAlias($current_path);

    // Alias exists, active trail should have been found already.
    if ($internalpath !== $current_path) {
      return $active_trail;
    }

    // Query to get alias based on beta alias.
    $query = $this->database->select('path_alias', 'pa');
    $query->condition('pa.beta_alias', $current_path, '=');
    $query->addField('pa', 'path', '=');

    $internalpath = $query->execute()->fetchField();

    if ($internalpath === FALSE) {
      return $active_trail;
    }

    // Get the route name based on the internal path.
    $internalurl = $this->pathValidator->getUrlIfValid($internalpath);
    $routename = $internalurl->getRouteName();
    $routeparams = $internalurl->getRouteParameters();
    $links = $this->menuLinkManager->loadLinksByRoute($routename, $routeparams, $menu_name);

    if ($links) {
      $found = reset($links);
      if ($parents = $this->menuLinkManager->getParentIds($found->getPluginId())) {
        $active_trail = $parents + $active_trail;
      }

    }

    return $active_trail;

  }

}
