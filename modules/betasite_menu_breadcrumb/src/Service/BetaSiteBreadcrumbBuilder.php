<?php

namespace Drupal\betasite_menu_breadcrumb\Service;

use Drupal\betasite\Service\BetaSiteManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\menu_breadcrumb\MenuBasedBreadcrumbBuilder;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Creates the breadcrumbs for the beta site.
 */
class BetaSiteBreadcrumbBuilder extends MenuBasedBreadcrumbBuilder {

  /**
   * Beta Site Manager.
   *
   * @var \Drupal\betasite\Service\BetaSiteManagerInterface
   */
  protected $betaSiteManager;

  /**
   * BetaSiteBreadcrumbBuilder constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory Interface.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   Menu Active Trail Interface.
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager
   *   Menu Link Manager Interface.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   Admin Context Service.
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   Title Resolver Interface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack Interface.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   Language Manager Interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_menu
   *   Cache Backend Interface.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   Lock Backend Interface.
   * @param \Drupal\betasite\Service\BetaSiteManagerInterface $betaSiteManager
   *   Beta Site Manager Service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    MenuActiveTrailInterface $menu_active_trail,
    MenuLinkManagerInterface $menu_link_manager,
    AdminContext $admin_context,
    TitleResolverInterface $title_resolver,
    RequestStack $request_stack,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager,
    CacheBackendInterface $cache_menu,
    LockBackendInterface $lock,
    BetaSiteManagerInterface $beta_site_manager
  ) {
    parent::__construct($config_factory, $menu_active_trail, $menu_link_manager, $admin_context, $title_resolver, $request_stack, $language_manager, $entity_type_manager, $cache_menu, $lock);
    $this->betaSiteManager = $beta_site_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function applies(RouteMatchInterface $route_match): bool {
    if (!$this->betaSiteManager->isBetaSite()) {
      return FALSE;
    }

    if (!$this->config->get('determine_menu')) {
      return FALSE;
    }
    // Don't breadcrumb the admin pages, if disabled on config options:
    if ($this->config->get('disable_admin_page') && $this->adminContext->isAdminRoute($route_match->getRouteObject())) {
      return FALSE;
    }
    // No route name means no active trail:
    $route_name = $route_match->getRouteName();
    if (!$route_name) {
      return FALSE;
    }

    // Make sure menus are selected, and breadcrumb text strings, are displayed
    // in the content rather than the (default) interface language:
    $this->contentLanguage = $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    // Hardcode the menu to the beta site main menu.
    $this->setMenuName('cms-global-primary-menu-2');
    $trail_ids = $this->menuActiveTrail->getActiveTrailIds($this->getMenuName());
    $trail_ids = array_filter($trail_ids);
    array_shift($trail_ids);
    $this->setMenuTrail($trail_ids);
    $this->taxonomyAttachment = NULL;
    return TRUE;
  }

}
