<?php

namespace Drupal\betasite_switches\TwigExtension;

use Drupal\betasite_switches\BetaSwitchManagerInterface;
use Psr\Log\LoggerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Beta Switches Twig extension that adds a custom function to check a switch status.
 *
 * @code
 * {{ betaswitch($my_switch_id) }}
 * @endcode
 */
class BetaSwitchExtension extends AbstractExtension {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Beta Switch Manager service.
   *
   * @var \Drupal\betasite_switches\BetaSwitchManagerInterface
   */
  protected $switchManager;

  /**
   * Constructs the BetaSwitchExtension object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\betasite_switches\BetaSwitchManagerInterface $switchManager
   *   The Beta Switch Manager service.
   */
  public function __construct(LoggerInterface $logger, BetaSwitchManagerInterface $switchManager) {
    $this->logger = $logger;
    $this->switchManager = $switchManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('betaswitch', [$this, 'betaSwitch']),
    ];
  }

  /**
   * Returns Activation status of beta switch if it exists.
   *
   * @param string $switchId
   *   The machine name for the beta switch.
   *
   * @return bool
   *   The beta switch status or the configured default for missing
   *   switches.
   *
   * @see BetaSwitchManagerInterface::getStatus()
   */
  public function betaSwitch(string $switchId): bool {
    $response = $this->switchManager->getStatus($switchId);
    return $response;
  }

}
