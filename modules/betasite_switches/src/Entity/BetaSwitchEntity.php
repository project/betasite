<?php

namespace Drupal\betasite_switches\Entity;

use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;

/**
 * Defines the Beta Switch entity.
 *
 * @ConfigEntityType(
 *   id = "beta_switch",
 *   label = @Translation("Beta Switch"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\betasite_switches\BetaSwitchListBuilder",
 *     "form" = {
 *       "add" = "Drupal\betasite_switches\Form\BetaSwitchForm",
 *       "edit" = "Drupal\betasite_switches\Form\BetaSwitchForm",
 *       "delete" = "Drupal\betasite_switches\Form\BetaSwitchDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\betasite_switches\BetaSwitchHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "beta_switch",
 *   admin_permission = "administer beta switches",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/config/development/betasite/switches/{beta_switch}",
 *     "add-form" = "/admin/config/development/betasite/switches/add",
 *     "edit-form" = "/admin/config/development/betasite/switches/{beta_switch}/edit",
 *     "delete-form" = "/admin/config/development/betasite/switches/{beta_switch}/delete",
 *     "collection" = "/admin/config/development/betasite/switches"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "value",
 *   },
 *   lookup_keys = {
 *     "id"
 *   }
 * )
 */
class BetaSwitchEntity extends ConfigEntityBase implements BetaSwitchInterface {

  /**
   * The Beta Switch ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Beta Switch label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Beta Switch description.
   *
   * @var string
   */
  protected $description;

  /**
   * The Beta Switch value.
   *
   * @var integer
   */
  protected $value;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return (int) $this->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value) {
    return $this->set('value', $value);
  }

}
