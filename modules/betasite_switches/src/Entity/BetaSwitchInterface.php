<?php

namespace Drupal\betasite_switches\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Beta Switch entities.
 */
interface BetaSwitchInterface extends ConfigEntityInterface {

  /**
   * Returns the beta switch description.
   *
   * @return string
   *   The beta switch description.
   */
  public function getDescription();

  /**
   * Gets the beta switch value.
   *
   * @return integer
   *   The beta switch value.
   */
  public function getValue();

  /**
   * Sets the beta switch value.
   *
   * @param integer $value
   *   The value.
   *
   * @return $this
   */
  public function setValue($value);

}
