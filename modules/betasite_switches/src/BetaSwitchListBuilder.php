<?php

namespace Drupal\betasite_switches;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Beta Switch entities.
 */
class BetaSwitchListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Beta Switch');
    $header['id'] = $this->t('Machine name');
    $header['value'] = $this->t('Value');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $statuses = [
      0 => $this->t('Disabled'),
      1 => $this->t('Default only'),
      2 => $this->t('Beta only'),
      3 => $this->t('Default and Beta'),
    ];

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $statuses[$entity->getValue()];
    return $row + parent::buildRow($entity);
  }

}
