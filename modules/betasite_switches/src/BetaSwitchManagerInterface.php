<?php

namespace Drupal\betasite_switches;

/**
 * Defines the required interface for the Beta Switch Manager service.
 */
interface BetaSwitchManagerInterface {

  /**
   * Loads a beta switch instance.
   *
   * @param string $switch_id
   *   The beta switch machine name.
   *
   * @return \Drupal\betasite_switches\Entity\BetaSwitchInterface
   *   The loaded and configured beta switch entity.
   *
   * @throws \Drupal\betasite_switches\Exception\MissingBetaSwitchException
   *   Exception thrown when an undefined Beta Switch is requested.
   */
  public function getSwitch($switch_id);

  /**
   * Returns status for switch.
   *
   * @param string $switch_id
   *   The beta switch machine name.
   *
   * @return integer
   *   The status for the specified beta switch entity.
   */
  public function getStatus($switch_id);

}
