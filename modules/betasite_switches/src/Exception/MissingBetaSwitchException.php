<?php

namespace Drupal\betasite_switches\Exception;

/**
 * Exception thrown when an undefined Beta Switch is requested.
 */
class MissingBetaSwitchException extends \Exception {

}
