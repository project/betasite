<?php

namespace Drupal\betasite_switches;

use Drupal\betasite\Service\BetaSiteManager;
use Drupal\betasite_switches\Exception\MissingBetaSwitchException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

/**
 * The beta switch manager service for interacting with beta switches.
 *
 * This service should be used for most interactions with beta switches
 * throughout the system.
 */
class BetaSwitchManager implements BetaSwitchManagerInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The storage handler for beta switches.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $betaSwitchStorage;

  /**
   * The logger channel for reporting important Beta Switch events.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Beta Site manager.
   *
   * @var \Drupal\betasite\Service\BetaSiteManager
   */
  protected $betaSiteManager;

  /**
   * BetaSwitchManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $loggerChannel
   *   The logger channel for reporting important Beta Switch events.
   * @param \Drupal\betasite\Service\BetaSiteManager $beta_site_manager
   *   The Beta Manager service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, LoggerChannelInterface $loggerChannel, BetaSiteManager $beta_site_manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->switchStorage = $entityTypeManager->getStorage('beta_switch');
    $this->logger = $loggerChannel;
    $this->betaSiteManager = $beta_site_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getSwitch($switch_id) {
    $switch = $this->switchStorage->load($switch_id);

    if (is_null($switch)) {
      throw new MissingSwitchException('Attempted to load a missing Beta Switch: ' . $switch_id);
    }

    return $switch;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus($switch_id) {
    try {
      $status = $this->getSwitch($switch_id)->getValue();

      if ($status === 3) {
        return TRUE;
      }

      $isBeta = $this->betaSiteManager->isBetaSite();

      if ($status === 2 && $isBeta) {
        return TRUE;
      }

      if ($status === 1 && !$isBeta) {
        return TRUE;
      }

      return FALSE;
    }
    catch (MissingSwitchException $exception) {
      // Log this as a warning for follow-up.
      $this->logger->warning($exception->getMessage());

      // Return the default status for missing beta switches.
      return $this->getDefaultStatus();
    }
  }

  /**
   * Get the default status for missing or disabled beta switches.
   *
   * @return integer
   *   The default status for missing or disabled beta switches.
   */
  protected function getDefaultStatus() {
    // @todo Make the default Switch value configurable.
    return 0;
  }

}
