<?php

namespace Drupal\betasite_switches\Form;

use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BetaSwitchForm.
 */
class BetaSwitchForm extends EntityForm {

  /**
   * The entity being used by this form.
   *
   * @var \Drupal\switches\Entity\SwitchEntity
   */
  protected $entity;

  /**
   * The Condition plugin manager service.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The Context Repository service.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * BetaSwitchForm constructor.
   *
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   The Condition plugin manager service.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $context_repository
   *   The Context Repository service.
   */
  public function __construct(ConditionManager $condition_manager,
                              ContextRepositoryInterface $context_repository) {
    $this->conditionManager = $condition_manager;
    $this->contextRepository = $context_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.condition'),
      $container->get('context.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Store the gathered contexts in the form state for other objects to use
    // during form building.
    $form_state->setTemporaryValue('gathered_contexts', $this->contextRepository->getAvailableContexts());

    // Nest the form state values.
    $form['#tree'] = TRUE;

    $beta_switch = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $beta_switch->label(),
      '#description' => $this->t("Label for the Beta Switch."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $beta_switch->id(),
      '#machine_name' => [
        'exists' => '\Drupal\betasite_switches\Entity\BetaSwitchEntity::load',
      ],
      '#disabled' => !$beta_switch->isNew(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('A summary of the intended purpose for this Beta Switch.'),
      '#maxlength' => 255,
      '#default_value' => $beta_switch->get('description'),
    ];

    $form['value'] = [
      '#type' => 'select',
      '#options' => [
        0 => $this->t('Disabled'),
        1 => $this->t('Default only'),
        2 => $this->t('Beta only'),
        3 => $this->t('Default and Beta'),
      ],
      '#title' => $this->t('Value'),
      '#default_value' => $beta_switch->getValue(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save the conditions configuration.
    $this->entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $beta_switch = $this->entity;
    $status = $beta_switch->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Beta Switch.', [
          '%label' => $beta_switch->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Beta Switch.', [
          '%label' => $beta_switch->label(),
        ]));
    }
    $form_state->setRedirectUrl($beta_switch->toUrl('collection'));
  }

}
