# Beta Site Switches

The module provides a UI where switches can be defined for site features to allow them to be
displayed on the default domain, the beta sub-domain or both. Both a service and a Twig
plugin are provided to allow calls from templates or other code to determine if a feature is
to be displayed or not.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/betasite)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/betasite)


## Table of contents

- Installation
- Maintainers


## Installation

1) Enable the module using the UI or "drush en betasite_switches"

2) Visit /admin/config/development/betasite/switches

3) Add a switch and select its status - note the machine name as you will need to pass
it to the service or Twig plugin as you see below using "new_feature_1" as an example.

4) In Twig templates, use code as follows to show/hide content:

{% if betaswitch('new_feature_1') == TRUE %}
  <!-- Markup for the new feature -->
{% else %}
  <!-- Original markup -->
{% endif %}

5) In other code, call the service as follows:

$switch = \Drupal::service('beta_switches.manager')->getStatus('new_feature_1');
if ($switch) {
  // New function.
} else {
  // Original function.
}

6) Check for the existence of a switch using EntityTypeManager:

if (\Drupal::entityTypeManager()->getStorage('beta_switch')->load('new_feature_1')) {
  // The switch exists.
}

For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Enable other sub-modules and their dependencies, as needed, for your project.


## Maintainers

Current maintainers:
- Cameron Prince - [cameron prince](https://www.drupal.org/u/cameronprince)
- Travis Lilleberg - [tlilleberg](https://www.drupal.org/u/tlilleberg)
- Sam Lerner - [SamLerner](https://www.drupal.org/u/SamLerner)
- Timothy Zura - [tzura](https://www.drupal.org/u/tzura)
