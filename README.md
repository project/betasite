# Beta Site

The module gives site owners the ability to access their Drupal sites using a second, "beta"
sub-domain and allows the same content to exist with different paths, the default alias for the
public site and an alternate, beta alias for the beta site.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/betasite)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/betasite)


## Table of contents

- Installation
- Maintainers


## Installation

1) Ensure your webserver configuration allows your site to be accessed using a "beta"
sub-domain. Drupal allows this by default, so there's no special configuration needed with it.
Production or public dev/testing environments will likely require a DNS entry for the beta
sub-domain. Local users may need to add an entry to the hosts file or a value in the config.yml
file for additional_hostnames when using DDEV.

2) Install the module using "composer require drupal/betasite"

3) Enable the module using the UI or "drush en betasite"

4) Edit nodes and add values in the new "Beta URL alias" input field

5) When you visit the site with the beta sub-domain, you should see the links to the nodes
updated to use the new aliases in menus

6) Use the new "Hostname" condition in the blocks UI to define which blocks should be shown or
hidden when the site is accessed with the beta sub-domain

7) Enable the "Beta Site Toggle Block" (drush en betasite_toggle_block) and place the block in
a region, typically above the content

8) Visit the site and use the link in the block to switch back and forth between beta

For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Enable other sub-modules and their dependencies, as needed, for your project.


## Maintainers

Current maintainers:
- Cameron Prince - [cameron prince](https://www.drupal.org/u/cameronprince)
- Travis Lilleberg - [tlilleberg](https://www.drupal.org/u/tlilleberg)
- Sam Lerner - [SamLerner](https://www.drupal.org/u/SamLerner)
- Timothy Zura - [tzura](https://www.drupal.org/u/tzura)
